use std::collections::HashMap;
use std::error::Error;
use std::io;

const DIGITS: [(&str, &str); 9] = [
    ("one", "1"),
    ("two", "2"),
    ("three", "3"),
    ("four", "4"),
    ("five", "5"),
    ("six", "6"),
    ("seven", "7"),
    ("eight", "8"),
    ("nine", "9"),
];

fn main() -> Result<(), Box<dyn Error>> {
    let input = io::read_to_string(io::stdin())?;
    let digitmap = HashMap::from(DIGITS.map(|(word, num)| (word.to_string(), num)));
    let revmap = HashMap::from(DIGITS.map(|(word, num)| (word.chars().rev().collect::<String>(), num)));

    let calibration: u64 = input.lines().map(|line| {
        format!("{}{}",
            find_first_digit(line.to_string(), &digitmap),
            find_first_digit(line.chars().rev().collect::<String>(), &revmap)
        ).parse::<u64>().unwrap_or(0)
    }).sum();

    println!("{}", calibration);
    Ok(())
}

fn find_first_digit<'a>(line: String, digitmap: &HashMap<String, &'a str>) -> &'a str {
    let mut result = "";
    let mut digit_pos = line.len();

    for (word, &num) in digitmap {
        for match_str in [word.as_str(), num] {
            if let Some(index) = line.find(match_str) {
                if index < digit_pos {
                    // new candidate
                    result = num;
                    digit_pos = index;
                }
            }
        }
    }

    result
}