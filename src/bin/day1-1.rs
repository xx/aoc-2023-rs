use std::io;
use std::error::Error;

const DIGITS: [char; 9] = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];

fn main() -> Result<(), Box<dyn Error>> {
    let input = io::read_to_string(io::stdin())?;

    let calibration = input.lines()
        .filter_map(|line| {
            let first = line.chars().find(|c| DIGITS.contains(c))?;
            let last = line.chars().rev().find(|c| DIGITS.contains(c))?;

            Some(format!("{}{}", first, last).parse::<u64>().ok()?)
        }).sum::<u64>();

    println!("{}", calibration);
    Ok(())
}