use std::error::Error;
use std::io;

fn main() -> Result<(), Box<dyn Error>> {
    let input = io::read_to_string(io::stdin())?;

    let sum: i32 = input.lines().map(|line| {
        line.split(":").last().unwrap().split(";").map(|shown| {
            let (mut r, mut g, mut b) = (0, 0, 0);
            for color in shown.split(",") {
                let mut color_iter = color.split_whitespace();
                let value: i32 = color_iter.next().unwrap().parse().unwrap();
                match color_iter.next() {
                    Some("red") => r = value,
                    Some("green") => g = value,
                    Some("blue") => b = value,
                    _ => println!("unknown color")
                }
            }
            (r, g, b)
        }).fold((0, 0, 0), |(r, g, b), (sr, sg, sb)| (r.max(sr), g.max(sg), b.max(sb)))
    }).map(|(r, g, b)| r * g * b).sum();

    println!("{}", sum);
    Ok(())
}