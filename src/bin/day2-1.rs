use std::error::Error;
use std::io;

fn main() -> Result<(), Box<dyn Error>> {
    let input = io::read_to_string(io::stdin())?;

    let sum: usize = input.lines().enumerate().map(|(i, line)| {
        if line.split(":").last().unwrap().split(";").map(|shown| {
            let (mut r, mut g, mut b) = (0, 0, 0);
            for color in shown.split(",") {
                let mut color_iter = color.split_whitespace();
                let value: i32 = color_iter.next().unwrap().parse().unwrap();
                match color_iter.next() {
                    Some("red") => r = value,
                    Some("green") => g = value,
                    Some("blue") => b = value,
                    _ => println!("unknown color")
                }
            }
            (r, g, b)
        }).all(|(r, g, b)| r <= 12 && g <= 13 && b <= 14)
        { i + 1 } else { 0 }
    }).sum();

    println!("{}", sum);
    Ok(())
}